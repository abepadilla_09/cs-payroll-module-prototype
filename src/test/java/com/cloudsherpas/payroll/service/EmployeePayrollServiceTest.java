package com.cloudsherpas.payroll.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.cloudsherpas.payroll.models.EmployeeSalaryDeductionModel;
import com.cloudsherpas.payroll.service.impl.EmployeePayrollServiceImpl;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;

@RunWith(MockitoJUnitRunner.class)
public class EmployeePayrollServiceTest {

    @Mock
    EmployeePayrollRulesService employeePayrollRulesService;
    
    @Mock
    EmployeePayrollDAO employeePayrollDAO;

    @InjectMocks
    EmployeePayrollService employeePayrollService = new EmployeePayrollServiceImpl();


    EmployeeDetailsDTO empDetails;
    EmployeePayrollDetailsDTO empPayrollDetails;
    EmployeePayrollModel empPayrollModel;

    @Before
    public void setUp() {
        setUpEmployeeDetails();
        setUpEmployeePayrollDetails();
        setUpEmployeePayrollModel();
    }
    
    private void setUpEmployeeDetails() {
        empDetails = new EmployeeDetailsDTO();
        empDetails.setEmployeeKey("empKey1");
        empDetails.setCountryCode("PH");
        empDetails.setFirstName("EmpFirstName");
        empDetails.setLastName("EmpLastName");
        empDetails.setNoOfAbsences(1L);
        empDetails.setNoOfLeaves(2L);
        empDetails.setJobPosition("Manager");
        empDetails.setTotalDaysPerCutoff(10);
    }
        
    private void setUpEmployeePayrollDetails() {
        empPayrollDetails = new EmployeePayrollDetailsDTO();
        empPayrollDetails.setDailyRate(2400L);
        empPayrollDetails.setEmployeeKey("empKey1");
        empPayrollDetails.setOvertimePay(1200L);
        EmployeePayrollDeductionsDTO deductions = new EmployeePayrollDeductionsDTO();
        deductions.setAbsencesDeduction(2000L);
        deductions.setIncomeTaxDeduction(1500L);
        deductions.setPagibigDeduction(100L);
        deductions.setPhilHealthDeduction(500L);
        deductions.setSssDeduction(300L);
        empPayrollDetails.setDeductions(deductions);
    }

    private void setUpEmployeePayrollModel() {
    	empPayrollModel = new EmployeePayrollModel();
    	empPayrollModel.setDailyRate(2400L);
    	empPayrollModel.setEmployeeKey("empKey1");
    	empPayrollModel.setOvertimePay(1200L);
        EmployeeSalaryDeductionModel deductions = new EmployeeSalaryDeductionModel();
        deductions.setAbsencesDeduction(2000L);
        deductions.setIncomeTaxDeduction(1500L);
        deductions.setPagibigDeduction(100L);
        deductions.setPhilHealthDeduction(500L);
        deductions.setSssDeduction(300L);
        empPayrollModel.setDeductions(deductions);
    }
    

    @Test
    public void testComputeSalaryForEmployee() {
        when(employeePayrollRulesService.getEmployeePayrollDetails(empDetails))
            .thenReturn(empPayrollDetails);
        EmployeePayrollDetailsDTO result = employeePayrollService.computeSalaryforEmployee(empDetails);
        verify(employeePayrollDAO, times(1)).put(Matchers.any(EmployeePayrollModel.class));
        verify(employeePayrollRulesService, times(1)).getEmployeePayrollDetails(empDetails);
       // Assert.assertEquals(20800l, result.getNetSalary().longValue());
        Assert.assertEquals(24000l, result.getGrossSalary().longValue());
    }

    @Test
    public void testComputeSalaryForEmployeeNull() {
    	empDetails.setEmployeeKey("");
        when(employeePayrollRulesService.getEmployeePayrollDetails(empDetails))
            .thenReturn(empPayrollDetails);
        EmployeePayrollDetailsDTO result = employeePayrollService.computeSalaryforEmployee(empDetails);
        verify(employeePayrollDAO, times(0)).put(Matchers.any(EmployeePayrollModel.class));
        verify(employeePayrollRulesService, times(0)).getEmployeePayrollDetails(empDetails);
       // Assert.assertEquals(20800l, result.getNetSalary().longValue());
        Assert.assertNull(result);
    }

    
    @Test
    public void testGetAlLSalary() {
    	List<EmployeePayrollModel> list = Lists.newArrayList();
    	list.add(empPayrollModel);
        when(employeePayrollDAO.getPayrollDetailsByEmployee(Matchers.anyString())).thenReturn(list);
        List<EmployeePayrollDetailsDTO> result = employeePayrollService.getAllSalary(Matchers.anyString());
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void testGetAlLSalaryNull() {
        when(employeePayrollDAO.getPayrollDetailsByEmployee(Matchers.anyString())).thenReturn(null);
        List<EmployeePayrollDetailsDTO> result = employeePayrollService.getAllSalary(Matchers.anyString());
        Assert.assertNull(result);
    }
}
