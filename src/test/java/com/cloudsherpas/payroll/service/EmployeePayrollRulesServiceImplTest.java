package com.cloudsherpas.payroll.service;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.service.impl.EmployeePayrollRulesServiceImpl;


public class EmployeePayrollRulesServiceImplTest {
    EmployeePayrollRulesServiceImpl employeePayrollService = new EmployeePayrollRulesServiceImpl();
    EmployeeDetailsDTO empDetails;
    EmployeePayrollDeductionsDTO employeeDeductions;
    EmployeePayrollDetailsDTO empPayrollDetails;
    
    @Before
    public void setUpEmployeeDetails() {
        empDetails = new EmployeeDetailsDTO();
        empDetails.setEmployeeKey("empKey1");
        empDetails.setCountryCode("PH");
        empDetails.setFirstName("EmpFirstName");
        empDetails.setLastName("EmpLastName");
        empDetails.setNoOfAbsences(5L);
        empDetails.setNoOfLeaves(2L);
        empDetails.setJobPosition("DEVELOPER");
        empDetails.setJobGradeLevel(1);
        empDetails.setTotalOvertimeHours(4L);
        empDetails.setTotalDaysPerCutoff(10);
        empPayrollDetails = employeePayrollService.getEmployeePayrollDetails(empDetails);
    }
    
    
    public void testGetEmployeeManagerDailyRate() {
        assertTrue(empPayrollDetails.getDailyRate()==5000L);
    }
    
    public void testGetEmployeeNonManagerialDailyRate() {
        EmployeeDetailsDTO empDetailLocal = new EmployeeDetailsDTO();
        empDetailLocal.setJobPosition("Mid Level Web Developer");
        empDetailLocal.setJobGradeLevel(4);
        empDetailLocal.setNoOfAbsences(1L);
        empDetailLocal.setNoOfLeaves(2L);
        EmployeePayrollDetailsDTO empPayrollDetailsLocal = employeePayrollService.getEmployeePayrollDetails(empDetailLocal);
        assertTrue(empPayrollDetailsLocal.getDailyRate()==800L);
    }
    
    public void testGetEmployeeTaxRate() {
        assertTrue(empPayrollDetails.getTaxRate()==32L);
    }
    
    public void testGetEmployeeOvertimeForManagerLevel(){
        assertTrue(empPayrollDetails.getOvertimePay() == 2500);
    }
    
    public void testGetEmployeeOvertimeForNonManagerLevel(){
        EmployeeDetailsDTO empDetailLocal = new EmployeeDetailsDTO();
        empDetailLocal.setJobPosition("Mid Level Web Developer");
        empDetailLocal.setJobGradeLevel(4);
        empDetailLocal.setTotalOvertimeHours(4L);
        empDetailLocal.setNoOfAbsences(1L);
        empDetailLocal.setNoOfLeaves(2L);
        EmployeePayrollDetailsDTO empPayrollDetailsLocal = employeePayrollService.getEmployeePayrollDetails(empDetailLocal);
        assertTrue(empPayrollDetailsLocal.getOvertimePay() == 400);
    }
    
    public void testGetGovernmentDeductions(){
        assertTrue(empPayrollDetails.getDeductions().getSssDeduction()  == 500L);
    }
    
    @Test
    public void testSample(){
        Assert.assertNotNull(empPayrollDetails.getMonthlyRate());
    }

}
;