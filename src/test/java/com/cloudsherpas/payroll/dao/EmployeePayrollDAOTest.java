package com.cloudsherpas.payroll.dao;

import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.payroll.dao.impl.EmployeePayrollDAOImpl;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.cloudsherpas.payroll.models.EmployeeSalaryDeductionModel;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;

@RunWith(MockitoJUnitRunner.class)
public class EmployeePayrollDAOTest {
	@Mock
	EmployeePayrollDAO employeePayrollDAO;

	@InjectMocks
	EmployeePayrollDAOImpl employeePayrollDAOimpl = new EmployeePayrollDAOImpl();

	EmployeePayrollModel empPayrollModel;

	@Before
	public void setUp() {
		setUpEmployeePayrollModel();
	}

	private void setUpEmployeePayrollModel() {
		empPayrollModel = new EmployeePayrollModel();
		empPayrollModel.setDailyRate(2400L);
		EmployeeSalaryDeductionModel deductions = new EmployeeSalaryDeductionModel();
		deductions.setAbsencesDeduction(2000L);
		deductions.setIncomeTaxDeduction(1500L);
		deductions.setPagibigDeduction(100L);
		deductions.setPhilHealthDeduction(500L);
		deductions.setSssDeduction(300L);
		empPayrollModel.setDeductions(deductions);
		empPayrollModel.setEmployeeKey("empKey1");
		empPayrollModel.setGrossSalary(30000L);
		empPayrollModel.setNetSalary(20000L);
		empPayrollModel.setOvertimePay(0L);
		empPayrollModel.setTaxRate(32L);
		empPayrollModel.setId(1234L);
	}

	@Test
	public void getPayrollDetailsByEmployeeTest() {
		List<EmployeePayrollModel> list = Lists.newArrayList();
		list.add(empPayrollModel);
		when(employeePayrollDAO.getPayrollDetailsByEmployee("empKey1"))
				.thenReturn(list);
		List<EmployeePayrollModel> resultList = employeePayrollDAO
				.getPayrollDetailsByEmployee("empKey1");
		Assert.assertEquals(1, resultList.size());
	}

	@Test
	public void getPayrollDetailsByEmployeeNullTest() {
		List<EmployeePayrollModel> list = Lists.newArrayList();
		list.add(empPayrollModel);
		when(employeePayrollDAO.getPayrollDetailsByEmployee("none"))
				.thenReturn(list);
		List<EmployeePayrollModel> resultList = employeePayrollDAO
				.getPayrollDetailsByEmployee("empKey1");
		Assert.assertEquals(0, resultList.size());
	}
}
