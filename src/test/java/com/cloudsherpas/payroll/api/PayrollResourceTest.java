package com.cloudsherpas.payroll.api;

import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.web.EmployeePayrollListReponseDTO;
import com.cloudsherpas.payroll.dto.web.EmployeePayrollResponseDTO;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;

@RunWith(MockitoJUnitRunner.class)
public class PayrollResourceTest {
	
    
    @Mock
    EmployeePayrollService employeePayrollService ;


    EmployeeDetailsDTO empDetails;
    EmployeePayrollDetailsDTO empResponsePayrollDetails;
    

    @InjectMocks
    PayrollResource payrollResourceApi = new PayrollResource();
    
    @Before
    public void setUp() {
        setUpEmployeeDetails();
        setUpEmployeePayrollResponseDetails();
    }
    
    private void setUpEmployeeDetails() {
        empDetails = new EmployeeDetailsDTO();
        empDetails.setEmployeeKey("143");
        empDetails.setCountryCode("PH");
        empDetails.setFirstName("Cicci");
        empDetails.setLastName("Miranda");
        empDetails.setNoOfAbsences(1L);
        empDetails.setNoOfLeaves(2L);
        empDetails.setJobPosition("Senior Web Developer");
        empDetails.setJobGradeLevel(5);
        empDetails.setTotalDaysPerCutoff(15);
    }
        
    private void setUpEmployeePayrollResponseDetails() {
    	empResponsePayrollDetails = new EmployeePayrollDetailsDTO();
    	empResponsePayrollDetails.setDailyRate(2400L);
    	empResponsePayrollDetails.setEmployeeKey("empKey1");
    	empResponsePayrollDetails.setOvertimePay(1200L);
  
        EmployeePayrollDeductionsDTO deductions = new EmployeePayrollDeductionsDTO();
        deductions.setAbsencesDeduction(2000L);
        deductions.setIncomeTaxDeduction(1500L);
        deductions.setPagibigDeduction(100L);
        deductions.setPhilHealthDeduction(500L);
        deductions.setSssDeduction(300L);
        empResponsePayrollDetails.setDeductions(deductions);
    }

    
    

    @Test
	public void computeEmployeeSalaryTest(){
    	when(employeePayrollService.computeSalaryforEmployee(empDetails))
        .thenReturn(empResponsePayrollDetails);
    	EmployeePayrollResponseDTO result = payrollResourceApi.computeEmployeeSalary(empDetails);
    	Assert.assertEquals(200, result.getStatusCode());
    	Assert.assertEquals(2400, result.getData().getDailyRate().longValue());
    	Assert.assertEquals(1200, result.getData().getOvertimePay().longValue());
    	Assert.assertEquals(2000, result.getData().getDeductions().getAbsencesDeduction().longValue());
    	Assert.assertEquals(1500, result.getData().getDeductions().getIncomeTaxDeduction().longValue());
    	Assert.assertEquals(100, result.getData().getDeductions().getPagibigDeduction().longValue());
    	Assert.assertEquals(500, result.getData().getDeductions().getPhilHealthDeduction().longValue());
    	Assert.assertEquals(300, result.getData().getDeductions().getSssDeduction().longValue());
    }
    
    @Test
   	public void getAllSalaryTest(){
    	
    	List<EmployeePayrollDetailsDTO> list = Lists.newArrayList();
    	list.add(empResponsePayrollDetails);
       	when(employeePayrollService.getAllSalary("empKey1"))
           .thenReturn(list);
       	EmployeePayrollListReponseDTO resultList = payrollResourceApi.getAllSalary(("empKey1"));
       	Assert.assertEquals(1, resultList.getData().size());
    }
}
