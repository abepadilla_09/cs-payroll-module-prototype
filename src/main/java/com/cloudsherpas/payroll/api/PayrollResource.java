package com.cloudsherpas.payroll.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;

import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.dto.web.EmployeePayrollListReponseDTO;
import com.cloudsherpas.payroll.dto.web.EmployeePayrollResponseDTO;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;

@Api(name = "payroll", version = "v1", description = "Sample API for Payroll Module")
public class PayrollResource {

    @Autowired
    @Qualifier("employeePayrollService")
    @Lazy
    private EmployeePayrollService employeePayrollService;

    @ApiMethod(name = "computeEmployeeSalary", path = "salary", httpMethod = ApiMethod.HttpMethod.POST)
    public EmployeePayrollResponseDTO computeEmployeeSalary(
            final EmployeeDetailsDTO employeeDTO) {
        EmployeePayrollDetailsDTO result = employeePayrollService.computeSalaryforEmployee(employeeDTO);
        EmployeePayrollResponseDTO response = 
                new EmployeePayrollResponseDTO();
        if (result != null) {
            response.setStatusCode(HttpStatus.OK.value());
            response.setStatusMessage("Success");
            response.setData(result);

        } else {
            response.setStatusCode(HttpStatus.BAD_REQUEST.value());
            response.setStatusMessage("Error");
        }
        return response;

    }

    @ApiMethod(name = "getAllSalary", path = "salary/all", httpMethod = ApiMethod.HttpMethod.GET)
    public EmployeePayrollListReponseDTO getAllSalary(
            @Named("id") final String key) {
        EmployeePayrollListReponseDTO response = new EmployeePayrollListReponseDTO();
        List<EmployeePayrollDetailsDTO> result = employeePayrollService.getAllSalary(key);
        if (CollectionUtils.isEmpty(result)) {
            response.setStatusCode(HttpStatus.OK.value());
            response.setStatusMessage("No Salary found.");
        } else {
            response.setStatusCode(HttpStatus.BAD_REQUEST.value());
            response.setStatusMessage("Success");
            response.setResultCount(result.size());
            response.setData(result);
        }
        return response;
    }
}
