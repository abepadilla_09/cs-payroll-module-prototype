package com.cloudsherpas.payroll.service;

import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;

public interface EmployeePayrollRulesService {
    
    EmployeePayrollDetailsDTO getEmployeePayrollDetails (final EmployeeDetailsDTO employeeDetails);

}
