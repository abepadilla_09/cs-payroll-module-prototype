package com.cloudsherpas.payroll.service.impl;

import java.util.List;

import org.drools.core.util.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.util.CollectionUtils;

import com.cloudsherpas.payroll.dao.EmployeePayrollDAO;
import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDeductionsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.models.EmployeePayrollModel;
import com.cloudsherpas.payroll.service.EmployeePayrollRulesService;
import com.cloudsherpas.payroll.service.EmployeePayrollService;
import com.google.appengine.repackaged.com.google.api.client.util.Lists;

public class EmployeePayrollServiceImpl implements EmployeePayrollService {

    @Autowired
    @Qualifier("employeePayrollRulesService")
    @Lazy
    private EmployeePayrollRulesService employeePayrollRulesService;
    
    @Autowired
    @Qualifier("employeePayrollDAO")
    @Lazy
    private EmployeePayrollDAO employeePayrollDAO;

    private ModelMapper modelMapper;

    public EmployeePayrollServiceImpl() {
        modelMapper = new ModelMapper();
    }
    @Override
    public EmployeePayrollDetailsDTO computeSalaryforEmployee(final EmployeeDetailsDTO employeeDTO) {
        EmployeePayrollDetailsDTO employeePayrollDetails = null;
        if (!StringUtils.isEmpty(employeeDTO.getEmployeeKey())) {
            employeePayrollDetails = employeePayrollRulesService.getEmployeePayrollDetails(employeeDTO);
            
            if(null != employeePayrollDetails){
                employeePayrollDetails.setEmployeeKey(employeeDTO.getEmployeeKey());
                computeGrossSalary(employeePayrollDetails, employeeDTO.getTotalDaysPerCutoff());
                computeNetSalary(employeePayrollDetails);
                employeePayrollDAO.put(modelMapper.map(employeePayrollDetails, EmployeePayrollModel.class));
            }
        }
        return employeePayrollDetails;
    }

    @Override
    public List<EmployeePayrollDetailsDTO> getAllSalary(final String key) {
        List<EmployeePayrollModel> results = employeePayrollDAO.getPayrollDetailsByEmployee(key);
        List<EmployeePayrollDetailsDTO> employeePayrollDetailsList = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(results)) {
            for (EmployeePayrollModel model : results) {
                employeePayrollDetailsList.add(modelMapper.map(model, EmployeePayrollDetailsDTO.class));
            }
            return employeePayrollDetailsList;
        }
    return null;
    }

    private void computeGrossSalary(final EmployeePayrollDetailsDTO employeePayrollDetails, final Integer totalDaysInCutoff) {
        Long grossSalary = employeePayrollDetails.getDailyRate() * totalDaysInCutoff;
        employeePayrollDetails.setGrossSalary(grossSalary);
    }

    private void computeNetSalary(final EmployeePayrollDetailsDTO employeePayrollDetails) {
        Long netSalary = employeePayrollDetails.getGrossSalary() - employeePayrollDetails.getDeductions().getTotalDeduction()
                + employeePayrollDetails.getOvertimePay();
        employeePayrollDetails.setNetSalary(netSalary);
    }
    
 
   
}
