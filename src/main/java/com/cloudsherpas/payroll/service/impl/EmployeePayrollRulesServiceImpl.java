package com.cloudsherpas.payroll.service.impl;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;

import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;
import com.cloudsherpas.payroll.service.EmployeePayrollRulesService;

public class EmployeePayrollRulesServiceImpl implements EmployeePayrollRulesService {

	private StatefulKnowledgeSession session;
	
    @Override
    public EmployeePayrollDetailsDTO getEmployeePayrollDetails (final EmployeeDetailsDTO employeeDetails) {

		KnowledgeBase knowledgeBase = createKnowledgeBaseFromXLS(employeeDetails.getCountryCode());
		session = knowledgeBase.newStatefulKnowledgeSession();
		EmployeePayrollDetailsDTO empPayrollDetails = new EmployeePayrollDetailsDTO();
		session.insert(employeeDetails);
		session.insert(empPayrollDetails);
		session.setGlobal("empPayrollDetails", empPayrollDetails);
		session.fireAllRules();

         return empPayrollDetails;

    }

    private KnowledgeBase readKnowledgeBase() throws Exception {
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(ResourceFactory.newClassPathResource("GetSalaryRules.drl"), ResourceType.DRL);
        KnowledgeBuilderErrors errors = kbuilder.getErrors();
        if (errors.size() > 0) {
            for (KnowledgeBuilderError error: errors) {
                System.err.println(error);
            }
            throw new IllegalArgumentException("Could not parse knowledge.");
        }
        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
        return kbase;
    }

    private KnowledgeBase createKnowledgeBaseFromXLS(final String countryCode) {
		DecisionTableConfiguration dtconf = KnowledgeBuilderFactory
				.newDecisionTableConfiguration();
		dtconf.setInputType(DecisionTableInputType.XLS);

		KnowledgeBuilder knowledgeBuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		System.out.println(countryCode);
		knowledgeBuilder.add(ResourceFactory
				.newClassPathResource("EMP_COMPENSATION_BENEFITS_RULES_" + countryCode + ".xls"),
				ResourceType.DTABLE, dtconf);

		if (knowledgeBuilder.hasErrors()) {
			throw new RuntimeException(knowledgeBuilder.getErrors().toString());
		}		

		KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
		knowledgeBase.addKnowledgePackages(knowledgeBuilder
				.getKnowledgePackages());
		return knowledgeBase;

    }
}
