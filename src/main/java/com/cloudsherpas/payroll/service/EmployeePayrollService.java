package com.cloudsherpas.payroll.service;

import java.util.List;

import com.cloudsherpas.payroll.dto.core.EmployeeDetailsDTO;
import com.cloudsherpas.payroll.dto.core.EmployeePayrollDetailsDTO;

public interface EmployeePayrollService {

    List<EmployeePayrollDetailsDTO> getAllSalary(final String key);

    EmployeePayrollDetailsDTO computeSalaryforEmployee(final EmployeeDetailsDTO employeeDTO);

}
