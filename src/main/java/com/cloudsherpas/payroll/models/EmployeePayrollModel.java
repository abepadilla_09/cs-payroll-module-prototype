package com.cloudsherpas.payroll.models;

import com.googlecode.objectify.annotation.Container;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class EmployeePayrollModel {

    @Id
    private Long id;
    @Index
    private String employeeKey;
    private Long dailyRate;
    private Long taxRate;
    private Long overtimePay;
    private Long grossSalary;
    private Long netSalary;
    private EmployeeSalaryDeductionModel deductions;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getEmployeeKey() {
        return employeeKey;
    }
    public void setEmployeeKey(String employeeKey) {
        this.employeeKey = employeeKey;
    }
    public Long getDailyRate() {
        return dailyRate;
    }
    public void setDailyRate(Long dailyRate) {
        this.dailyRate = dailyRate;
    }
    public Long getTaxRate() {
        return taxRate;
    }
    public void setTaxRate(Long taxRate) {
        this.taxRate = taxRate;
    }
    public Long getOvertimePay() {
        return overtimePay;
    }
    public void setOvertimePay(Long overtimePay) {
        this.overtimePay = overtimePay;
    }
    public Long getGrossSalary() {
        return grossSalary;
    }
    public void setGrossSalary(Long grossSalary) {
        this.grossSalary = grossSalary;
    }
    public Long getNetSalary() {
        return netSalary;
    }
    public void setNetSalary(Long netSalary) {
        this.netSalary = netSalary;
    }
    public EmployeeSalaryDeductionModel getDeductions() {
        return deductions;
    }
    public void setDeductions(EmployeeSalaryDeductionModel deductions) {
        this.deductions = deductions;
    }
}
