package com.cloudsherpas.payroll.models;

import com.googlecode.objectify.annotation.Entity;

public class EmployeeSalaryDeductionModel {

    private Long incomeTaxDeduction;
    private Long sssDeduction;
    private Long philHealthDeduction;
    private Long pagibigDeduction;
    private Long absencesDeduction;

    public Long getIncomeTaxDeduction() {
        return incomeTaxDeduction;
    }
    public void setIncomeTaxDeduction(Long incomeTaxDeduction) {
        this.incomeTaxDeduction = incomeTaxDeduction;
    }
    public Long getSssDeduction() {
        return sssDeduction;
    }
    public void setSssDeduction(Long sssDeduction) {
        this.sssDeduction = sssDeduction;
    }
    public Long getPhilHealthDeduction() {
        return philHealthDeduction;
    }
    public void setPhilHealthDeduction(Long philHealthDeduction) {
        this.philHealthDeduction = philHealthDeduction;
    }
    public Long getPagibigDeduction() {
        return pagibigDeduction;
    }
    public void setPagibigDeduction(Long pagibigDeduction) {
        this.pagibigDeduction = pagibigDeduction;
    }
    public Long getAbsencesDeduction() {
        return absencesDeduction;
    }
    public void setAbsencesDeduction(Long absencesDeduction) {
        this.absencesDeduction = absencesDeduction;
    }

}
