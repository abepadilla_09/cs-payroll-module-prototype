package com.cloudsherpas.payroll.dto.core;

import java.util.Date;

public class EmployeeDetailsDTO {

    private String employeeKey;
    private String firstName; //remove
    private String lastName; //remove
    private Long totalOvertimeHours;
    private String countryCode;
    private Long noOfAbsences;
    private Long noOfLeaves; //remove
    private String jobPosition;
    private int jobGradeLevel;
    private Integer totalDaysPerCutoff; //remove

    //new
    private String civilStatus;
    private String jobPositionCode;
    private int numberOfDependents;
    private Double numberOfPaidTimeoffs;
    private Double numberOfAbsences;
    private Double numberOfDaysWorked;

    public String geplotEmyeeKey() {
        return employeeKey;
    }
    public void setEmployeeKey(final String employeeKey) {
        this.employeeKey = employeeKey;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(final String countryCode) {
        this.countryCode = countryCode;
    }
    public Long getNoOfAbsences() {
        return noOfAbsences;
    }
    public void setNoOfAbsences(final Long noOfAbsences) {
        this.noOfAbsences = noOfAbsences;
    }
    public Long getNoOfLeaves() {
        return noOfLeaves;
    }
    public void setNoOfLeaves(final Long noOfLeaves) {
        this.noOfLeaves = noOfLeaves;
    }
    public String getJobPosition() {
        return jobPosition;
    }
    public void setJobPosition(final String jobPosition) {
        this.jobPosition = jobPosition;
    }
    
    public int getJobGradeLevel() {
        return jobGradeLevel;
    }
    public void setJobGradeLevel(int jobGradeLevel) {
        this.jobGradeLevel = jobGradeLevel;
    }
    public Long getTotalOvertimeHours() {
        return totalOvertimeHours;
    }
    public void setTotalOvertimeHours(Long totalOvertimeHours) {
        this.totalOvertimeHours = totalOvertimeHours;
    }
    public Integer getTotalDaysPerCutoff() {
        return totalDaysPerCutoff;
    }
    public void setTotalDaysPerCutoff(Integer totalDaysPerCutoff) {
        this.totalDaysPerCutoff = totalDaysPerCutoff;
    }
    public String getEmployeeKey() {
        return employeeKey;
    }
    public String getCivilStatus() {
        return civilStatus;
    }
    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }
    public String getJobPositionCode() {
        return jobPositionCode;
    }
    public void setJobPositionCode(String jobPositionCode) {
        this.jobPositionCode = jobPositionCode;
    }
    public int getNumberOfDependents() {
        return numberOfDependents;
    }
    public void setNumberOfDependents(int numberOfDependents) {
        this.numberOfDependents = numberOfDependents;
    }
    public Double getNumberOfPaidTimeoffs() {
        return numberOfPaidTimeoffs;
    }
    public void setNumberOfPaidTimeoffs(Double numberOfPaidTimeoffs) {
        this.numberOfPaidTimeoffs = numberOfPaidTimeoffs;
    }
    public Double getNumberOfAbsences() {
        return numberOfAbsences;
    }
    public void setNumberOfAbsences(Double numberOfAbsences) {
        this.numberOfAbsences = numberOfAbsences;
    }
    public Double getNumberOfDaysWorked() {
        return numberOfDaysWorked;
    }
    public void setNumberOfDaysWorked(Double numberOfDaysWorked) {
        this.numberOfDaysWorked = numberOfDaysWorked;
    }

}
