package com.cloudsherpas.payroll.dto.core;

public class EmployeePayrollDetailsDTO {

    private String employeeKey;
    private Long dailyRate;
    private Long taxRate;
    private Long overtimePay;
    private Long grossSalary;
    private Long netSalary;
    private EmployeePayrollDeductionsDTO deductions;

    //news
    private Long monthlyRate;
    private Long hourlyRate;
    //allowances
    private Long gasAllowance;
    private Long communicationAllowance;
    private Long riceAllowance;
    private Long clothingAllowance;

    public String getEmployeeKey() {
        return employeeKey;
    }
    public void setEmployeeKey(String employeeKey) {
        this.employeeKey = employeeKey;
    }
    public Long getDailyRate() {
        return dailyRate;
    }
    public void setDailyRate(Long dailyRate) {
        this.dailyRate = dailyRate;
    }
    public EmployeePayrollDeductionsDTO getDeductions() {
        return deductions;
    }
    public void setDeductions(EmployeePayrollDeductionsDTO deductions) {
        this.deductions = deductions;
    }
    public Long getGrossSalary() {
        return grossSalary;
    }
    public void setGrossSalary(Long grossSalary) {
        this.grossSalary = grossSalary;
    }
    public Long getNetSalary() {
        return netSalary;
    }
    public void setNetSalary(Long netSalary) {
        this.netSalary = netSalary;
    }
    public Long getTaxRate() {
        return taxRate;
    }
    public void setTaxRate(Long taxRate) {
        this.taxRate = taxRate;
    }
    public Long getOvertimePay() {
        return overtimePay;
    }
    public void setOvertimePay(Long overtimePay) {
        this.overtimePay = overtimePay;
    }
    public Long getMonthlyRate() {
        return monthlyRate;
    }
    public void setMonthlyRate(Long monthlyRate) {
        this.monthlyRate = monthlyRate;
    }
    public Long getHourlyRate() {
        return hourlyRate;
    }
    public void setHourlyRate(Long hourlyRate) {
        this.hourlyRate = hourlyRate;
    }
    public Long getGasAllowance() {
        return gasAllowance;
    }
    public void setGasAllowance(Long gasAllowance) {
        this.gasAllowance = gasAllowance;
    }
    public Long getCommunicationAllowance() {
        return communicationAllowance;
    }
    public void setCommunicationAllowance(Long communicationAllowance) {
        this.communicationAllowance = communicationAllowance;
    }
    public Long getRiceAllowance() {
        return riceAllowance;
    }
    public void setRiceAllowance(Long riceAllowance) {
        this.riceAllowance = riceAllowance;
    }
    public Long getClothingAllowance() {
        return clothingAllowance;
    }
    public void setClothingAllowance(Long clothingAllowance) {
        this.clothingAllowance = clothingAllowance;
    }
}
